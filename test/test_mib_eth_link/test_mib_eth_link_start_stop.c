/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <netmodel/client.h>

#include "mib_eth.h"
#include "test_mib_start_stop.h"
#include "dummy_be.h"

static amxb_bus_ctx_t* bus_ctx = NULL;

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_setup(UNUSED void** state) {
    assert_int_equal(test_register_dummy_be(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);

    assert_int_equal(test_load_dummy_remote("eth_link.odl"), 0);
    assert_int_equal(test_load_dummy_remote("netmodel.odl"), 0);

    assert_true(netmodel_initialize());

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy_be(), 0);

    netmodel_cleanup();

    return 0;
}

void test_can_start_mib(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* eth_link = NULL;
    amxd_object_t* netmodel_interface = NULL;
    amxc_var_t params;
    amxd_trans_t trans;

    eth_link = amxd_dm_findf(test_get_dm(), "Ethernet.Link.ETH0.");
    assert_non_null(eth_link);
    netmodel_interface = amxd_dm_findf(test_get_dm(), "NetModel.Intf.ethLink-ETH0.");
    assert_non_null(netmodel_interface);

    // check initial values
    amxc_var_init(&params);
    assert_int_equal(amxd_object_get_params(netmodel_interface, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "MACAddress"), "");

    // init mib
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.ethLink-ETH0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    mib_added(NULL, &data, NULL);

    // make sure values are synced
    assert_int_equal(amxd_object_get_params(netmodel_interface, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "MACAddress"), "02:42:AC:11:00:03");

    // change values in Ethernet and verify that the NetModel values are synced
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, eth_link);
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "02:42:AC:11:00:04");
    assert_int_equal(amxd_trans_apply(&trans, test_get_dm()), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_int_equal(amxd_object_get_params(netmodel_interface, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "MACAddress"), "02:42:AC:11:00:04");

//cleanup:
    amxc_var_clean(&data);
    amxc_var_clean(&params);
}

void test_can_stop_mib(UNUSED void** state) {
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.ethLink-ETH0.");

    mib_removed(NULL, &data, NULL);

    amxc_var_clean(&data);
}
