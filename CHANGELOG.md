# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.3 - 2023-03-24(16:46:48 +0000)

### Fixes

- Fix netmodel-ethernet unit tests

## Release v1.0.2 - 2022-04-05(06:44:51 +0000)

### Fixes

- The loopback interface must be marked with the netdev flag.

## Release v1.0.1 - 2022-03-28(13:57:25 +0000)

### Fixes

- The loopback interface must be marked with the netdev flag.

## Release v1.0.0 - 2022-02-28(11:11:33 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.3.0 - 2022-02-16(10:20:31 +0000)

### New

- The Upstream parameter must be mapped to the upstream netmodel flag

## Release v0.2.1 - 2022-02-14(15:01:46 +0000)

### Fixes

- only the NetDev client should synchronize NetDevName

## Release v0.2.0 - 2022-02-01(11:48:40 +0000)

### New

- Integrate support for bridging in NetModel

## Release v0.1.3 - 2022-01-27(11:56:25 +0000)

### Fixes

- wrong mib can be unsubscribed when netmodel clients share a netmodel interface

## Release v0.1.2 - 2022-01-21(16:25:00 +0000)

### Changes

- expand libnetmodel for mibs/netmodel-clients

## Release v0.1.1 - 2022-01-04(08:20:54 +0000)

### Fixes

- create mib_ethernet

## Release v0.1.0 - 2021-12-22(15:04:43 +0000)

### New

- create mib_ethernet

